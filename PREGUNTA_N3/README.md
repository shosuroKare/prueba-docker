## Pregunta 3

Se corre el container de my-sql, al no estar instalado este se descarga automaticamente,
`docker run --name=zippy_test_mysql -e MYSQL_ROOT_PASSWORD=admin -d mysql/mysql-server:latest`

se le asigna una contraseña al momento de crear el servidor y este se corre en segundo plano por el `-d`
corremos el siguiente comando para ingresar al contenedor:
`docker exec -it zippy_test_mysql mysql -uroot -p`

se crea un usuario nuevo `CREATE USER 'admin_zippy'@'localhost' IDENTIFIED BY 'password';`

se le entregan privilegios de SU
`GRANT ALL PRIVILEGES ON * . * TO 'admin_zippy'@'localhost';`
