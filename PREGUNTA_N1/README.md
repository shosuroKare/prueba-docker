se construye la imagen con
`docker build -t test_zippy .`

y corremos nuestro contenedor con
`docker run -p 8080:80 -d test_zippy`

con esto se ejecuta el contenedor en el puerto 8080 en segundo plano.
